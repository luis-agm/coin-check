import { Root } from "./types";

const options = new URLSearchParams({
  slug: "bitcoin,ethereum,litecoin"
})

const coinsData = await fetch(`${process.env.CMC_URL}v2/cryptocurrency/quotes/latest?${options.toString()}`, {
  headers: {
    "X-CMC_PRO_API_KEY": process.env.CMC_API_KEY,
  }
});

const { data }: Root = await coinsData.json()

for (const stuff in data) {
  const coin = data[stuff]
  const {price} = coin.quote.USD
  console.log(coin.name , `$${price.toFixed(2)}`) 
}


export {}